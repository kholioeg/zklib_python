from struct import pack, unpack
from datetime import datetime, date

from zkconst import *

def getSizeAttendance(self):
    """Checks a returned packet to see if it returned CMD_PREPARE_DATA,
    indicating that data packets are to be sent

    Returns the am-ount of bytes that are going to be sent"""
    command = unpack('HHHH', self.data_recv[:8])[0] 
    if command == CMD_PREPARE_DATA:
        size = unpack('I', self.data_recv[8:12])[0]
        return size
    else:
        return False


def reverseHex(hexstr):
    tmp = ''
    for i in reversed( xrange( len(hexstr)/2 ) ):
        tmp += hexstr[i*2:(i*2)+2]
    
    return tmp
    
def zkgetattendance(self):
    """Start a connection with the time clock"""
    command = CMD_ATTLOG_RRQ
    command_string = ''
    chksum = 0
    session_id = self.session_id
    reply_id = unpack('HHHH', self.data_recv[:8])[3]

    buf = self.createHeader(command, chksum, session_id,
        reply_id, command_string)
    self.zkclient.sendto(buf, self.address)
    print buf.encode("hex")
    try:
        print "send data buffer"
        self.data_recv, addr = self.zkclient.recvfrom(1024)
        
        if getSizeAttendance(self):
            bytes = getSizeAttendance(self)
            print "Bytes: ", bytes
            while bytes > 0:
		# original recvfrom was 1032
                try:
                    data_recv, addr = self.zkclient.recvfrom(1032)
                    
                    self.attendancedata.append(data_recv)
                    #x = data_recv
                    #print x.encode('hex')
                    bytes -= 1024
                    # print "New Bytes: ", bytes
                except:
                    print "Error setting data_recv"
            self.session_id = unpack('HHHH', self.data_recv[:8])[2]
            print self.session_id
            data_recv = self.zkclient.recvfrom(8)
	    print "zkclient data: ", data_recv
	    print "attendancedata length: ", len(self.attendancedata)
            
        
        attendance = []
         
        if len(self.attendancedata) > 0:
            # The first 4 bytes don't seem to be related to the user
            for x in xrange(len(self.attendancedata)):
                #print"X range: ", x
                if x > 0:
                    
                    self.attendancedata[x] = self.attendancedata[x][8:]
                    
            attendancedata = ''.join( self.attendancedata )
            attendancedata = attendancedata[8:]
            
            while len(attendancedata) > 8:
                try:
                    space, uid, space1, timestamp, status, flag, space2 = unpack('8s2s6s8s4s2s2s', attendancedata.ljust(16)[:16].encode('hex'))
                except:
                    print "error unpacking!"
                #timestamp, status, flag, space, uid, space1 = unpack( '8s4s2s10s2s6s', attendancedata.ljust(16)[:16].encode('hex') )
                
                #
                # 3800bb1b0101ff0000000000d8000000
                #
                try:
                    attendance.append((int(uid, 16), int(status, 16), decode_time(int(reverseHex(timestamp), 16))))
                except:
                    print "error appending!"
               
                attendancedata = attendancedata[16:]
                
        return attendance
    except:
        return False
    
    
def zkclearattendance(self):
    """Start a connection with the time clock"""
    command = CMD_CLEAR_ATTLOG
    command_string = ''
    chksum = 0
    session_id = self.session_id
    reply_id = unpack('HHHH', self.data_recv[:8])[3]

    buf = self.createHeader(command, chksum, session_id,
        reply_id, command_string)
    self.zkclient.sendto(buf, self.address)
    #print buf.encode("hex")
    try:
        self.data_recv, addr = self.zkclient.recvfrom(1024)
        self.session_id = unpack('HHHH', self.data_recv[:8])[2]
        return self.data_recv[8:]
    except:
        return False
